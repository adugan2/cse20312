#ifndef DOUBLEHASH_H
#define DOUBLEHASH_H

#include "LinearProbe.h"

template<class Key, class Value>
class DoubleHash : public HashTable<Key, Value>{
	
	private:
	
		unsigned int findPos(Key theKey) const{
			
			unsigned int currentPos;
			unsigned int iter = 1;
			
			do{
				unsigned int intermediate1 = HashFunc(theKey)%(unsigned int)this->capacity();
				unsigned int intermediate2 = iter*HashFunc2(theKey);
				currentPos = (intermediate1 + intermediate2) % (unsigned int)this->capacity();
				++iter;
			}
			while(
				this->array[ currentPos ].state != EMPTY
				&& this->array[ currentPos ].state != DELETED
				&& this->array[ currentPos ].key != theKey
				&& iter <= this->capacity()
			);
			
			// Return capacity if the current value isn't the key. For safety
			if(this->array.at( currentPos ).state == ACTIVE 
				&& this->array.at( currentPos ).key != theKey ){
					
					return (unsigned int)this->capacity();
					
				}
			
			return currentPos;
			
		}
	
	
	public:
	
		DoubleHash(const unsigned int size = 0) : HashTable<Key, Value>(size) {}
		
	
		friend std::ostream& operator<<(std::ostream& output, const DoubleHash<Key,Value>& hash){
			
			output << "# of Hashed Elements: " << hash.numHash << " ";
			output << "Hash Capacity: " << hash.array.capacity() << std::endl;
			
			for(unsigned int iter = 0 ; iter < hash.capacity(); ++iter){
				
				output << "{"<< iter << ", ";
				
				if(hash.array[ iter ].state == ACTIVE){
					output << "ACTIVE, " << hash.array[iter].element;
				}
				else if(hash.array[ iter ].state == EMPTY){
					output << "EMPTY, ";
				}
				else{
					output << "DELETED, " << hash.array[iter].element;
				}
				
				output << "}";
				
				std::cout << std::endl;
			}
			
			return output;
		}
	
};

#endif