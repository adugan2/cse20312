/**********************************************
* File: BookFreq.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is a solution to the Word Frequency question 
* Problem 1 of Lecture 14
**********************************************/
#include <iostream>			// std::cout
#include <fstream>			// std::ifstream
#include <unordered_map>	// Hash Table
#include <string>			// Words

int totalWords = 0;

/********************************************
* Function Name  : readWords
* Pre-conditions : std::unordered_map< std::string, int >& HashWords, std::ifstream& bookFile
* Post-conditions: none
* 
* Reads from the book file and updates the total number of words 
********************************************/
void readWords(std::unordered_map< std::string, int >& HashWords, std::ifstream& bookFile){
	
	// Get the strings and put in the Hash Table
	std::string wordIn;
	while (bookFile >> wordIn)
	{
		// If the word has not yet been hashed
		if(HashWords.count(wordIn) == 0){
			
			// Put the word in, and set the count to 1
			HashWords.insert( {wordIn, 1} );
		}
		else{
			// Increment the count 
			HashWords[wordIn]++;
			totalWords++;
		}
	}	
}


/********************************************
* Function Name  : testWords
* Pre-conditions : std::unordered_map< std::string, int >& HashWords
* Post-conditions: none
* 
* Runs a set of test words to print the results 
********************************************/
void testWords(std::unordered_map< std::string, int >& HashWords){
	
	std::cout << "Lear occurs " << HashWords["Lear"] << " times\n";
	std::cout << "If occurs " << HashWords["If"] << " times\n";
	std::cout << "malmsey-nose occurs " << HashWords["malmsey-nose"] << " times\n";
	std::cout << "Romeo occurs " << HashWords["Romeo"] << " times\n";
	std::cout << "Juliet occurs " << HashWords["Juliet"] << " times\n";
	
}


/********************************************
* Function Name  : findMostCommon
* Pre-conditions : const std::unordered_map< std::string, int >& HashWords
* Post-conditions: none
* 
* Prints the most common word in the Hash 
********************************************/
void findMostCommon(const std::unordered_map< std::string, int >& HashWords){
	
	// Set a std::string and an int 
	int largestInt = 0;
	std::string largestStr = "";
	
	// Use the smart iterator for each word
	for(std::pair<const std::string, int> thePair : HashWords){
		
		if(thePair.second > largestInt){
			
			largestInt = thePair.second;
			largestStr = thePair.first;
		
		}
		
	}
	
	std::cout << "The most common word in document is '" << largestStr;
	std::cout << "', which occurs " << largestInt << " times!" << std::endl;
	
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function for the program  
********************************************/
int main(int argc, char** argv){

	// Create a HashMap using a list of integers as the values
	// The list of values represents the location in the array
	std::unordered_map< std::string, int > HashWords;
	
	if(argc != 2){
		
		std::cout << "Requires two inputs: ./BookFreq [Book Text]\n";
		exit(-1);
	}
	
	// Get the inputstream (Needs both file name and std::ifstream::in
	std::ifstream bookFile(argv[1], std::ifstream::in);
	if(!bookFile.is_open()){
		std::cout << "The file " << argv[1] << " does not exist. Exiting Program..." << std::endl;
		exit(-1);
	}
	
	// Read and increment the words into the BookFile
	readWords(HashWords, bookFile);

	// Close the ifstream
	bookFile.close();
	
	// How many total words
	std::cout << "There are " << totalWords << " words in " << argv[1] << (char)10;
	
	// Print four select words 
	testWords(HashWords);
	
	// Find the Most Common Words
	findMostCommon(HashWords);
	
	return 0;

}

