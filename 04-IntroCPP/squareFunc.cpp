/**********************************************
* File: squareFunc.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*
* This file contains two functions for summing
* an integer, and is used in lecture to demonstrate
* how the stack and variables work
**********************************************/
#include <iostream>

int total; // Global variable

int Square(int x){
	x = x * x;
	return x;
}

int SumOfSquare(int x, int y){
	int z = Square(x + y);
	return z;
}

int main(void){
	int x = 3, y = 4;
	total = SumOfSquare(x, y);
	std::cout << "The square of the sum of " << x << " and " << y << " is " << total << char(10);
	return 0;
}
