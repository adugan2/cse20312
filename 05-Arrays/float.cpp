/**********************************************
* File: float.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a fundamental example of floats  
**********************************************/

#include <iostream>
#include <iomanip>	/* Included for std::setprecision */
#include <cmath>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* These type cast examples show the importance of understand the 
	* types of information. When the user types in -1.0125, the computing device 
	* automatically makes it a double, so the user has to cast it to a float 
	* in order to avoid implicit type conversion. */
	float smallFloat = (float)-1.0125 * (float)pow(2, -127);
	float slideFloat = (float)0.15625;
	float largeFloat = (float)pow(2, 127);
	
	std::cout << "Decimal Representation of smallFloat is: " << smallFloat << std::endl;
	std::cout << "Decimal Representation of slideFloat is: " << slideFloat << std::endl;
	std::cout << "Decimal Representation of largeFloat is: " << largeFloat << std::endl;

	return 0;
}


