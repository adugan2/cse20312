/**********************************************
* File: money.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of structure of  
* money as a double, and why that may be problematic
**********************************************/
#include <iostream>
#include <iomanip>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable Declarations */
	double yourMoney = 100.15;
	yourMoney -= 0.18;
	
	std::cout << "Your Money at precision of 6 " 
		<< std::setprecision(4) << yourMoney << (char)10;
	std::cout << "Your Money at precision of 20 " 
		<< std::setprecision(20) << yourMoney << std::endl;
	
	return 0;
}

