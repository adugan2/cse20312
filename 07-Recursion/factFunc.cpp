
#include "factFunc.h"

/********************************************
* Function Name  : noBadChars
* Pre-conditions : std::string testString
* Post-conditions: bool
* 
* Returns false if any character other than 0-9
* '.', or '-' appears in the string
********************************************/
bool noBadChars(std::string testString){
	
	unsigned int i;
	for(i = 0; i < testString.length(); i++){
		
		/* Return false if - is outside of the first char */
		if(  (testString[i] < '0' || testString[i] > '9') ){
					
				/* Return false if negative sign is not at i == 0 */
				if(testString[i] == '-'){
					
					if(i != 0)
						return false;
					
				}
				
				/* Do not return false if there is a . here */
				else if(testString[i] != '.'){
					return false;
				} 
		}
		
	}
	
	return true;
}


/********************************************
* Function Name  : getValue
* Pre-conditions : std::string strUnsigX, unsigned int& unsigX
* Post-conditions: none
* 
* Overloaded C++ Function that tests inputs for unsigned int 
********************************************/
void getValue(std::string strUnsigX, unsigned int& unsigX){

	//std::cout << "Input an unsigned integer: ";	
	//std::cin >> strUnsigX;
	
	/* Check if the int input has no chars AND if the string contains . AND if the string contains -*/
	if(std::stringstream(strUnsigX) >> unsigX 
		&& noBadChars(strUnsigX)
		&& strUnsigX.find(".") == std::string::npos 
		&& strUnsigX.find("-") == std::string::npos){
			
		std::cout << "The unsigned integer value is " << unsigX << std::endl;
	
	}
	else{
		
		std::cerr << strUnsigX << " is not a valid unsigned Integer" << std::endl;
		exit(-1);
		
	}
}


/********************************************
* Function Name  : factorial
* Pre-conditions : unsigned int i
* Post-conditions: double
*  
* Calculates i!
********************************************/
double factorial(unsigned int i){
	
	std::cout << "Recursive call for " << i << std::endl;
	
	// Base case i = 0
	if(i == 0){
		std::cout << "Base Case found!" << std::endl;
		std::cout << "Base Case Result: " << i << std::endl;
		return 1;
	}
	
	// Recursive case
	double ret = (double)i*factorial(i-1);
	
	// Printed to show students
	std::cout << "Recursive Result: " << ret << std::endl;
	return ret;	// return i * factorial(i-1);
}
