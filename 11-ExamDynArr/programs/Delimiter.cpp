/**********************************************
* File: Delimiter.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is the main driver program for Problem 2
* in Lecture 11 Flipped 
**********************************************/

#include <string>		// String Storage
#include <vector>		// Dynamic Array
#include <iostream>		// Print to Terminal
#include <cstdlib>		// cerr and exit(-1)

const int numInputs = 3;

int main(int argc, char** argv){

	if(argc != numInputs){
		std::cerr << "Incorrect Inputs" << std::endl;
		exit(-1);
	}
	
	// Get delimiter and full String
	char delimiter = argv[1][0];
	std::string fullStr = argv[2];
	
	// Allocate Dynamic Array for Substrings
	std::vector< std::string > subStrings;
	
	// Iterate through the characters in the string and get substrings
	unsigned int sub_iter = 0;
	unsigned int place = 0;
	for(unsigned int iter = 0; iter < fullStr.size(); iter++){
		
		// Finds delimiter case
		if(fullStr[iter] == delimiter){
			subStrings.push_back( fullStr.substr(place, sub_iter) );
			place = iter+1;
			sub_iter = 0;
		}
		// Reaches end of string case
		else if(iter == fullStr.size() - 1){
			subStrings.push_back( fullStr.substr(place, sub_iter + 1) );
		}
		// Just iterate if not the end or a delimiter
		else{
			sub_iter++;
		}
		
	}
	
	// Use smart iterator for vector
	for(std::string& subStr : subStrings){
		std::cout << "\"" << subStr << "\"" << std::endl;
	}
	
	return 0;
}
