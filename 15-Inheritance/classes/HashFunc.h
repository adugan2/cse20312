/**********************************************
* File: HashFunc.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains Hash Functions for different data types 
**********************************************/

#ifndef HASHFUNC_H
#define HASHFUNC_H

/********************************************
* Function Name  : HashFunc
* Pre-conditions : int Value 
* Post-conditions: int
* 
* Returns a Hash Value for integer 
********************************************/
int HashFunc( int Value ){
	
	return Value;
}


/********************************************
* Function Name  : HashFunc
* Pre-conditions : int Value 
* Post-conditions: int
* 
* Second Hash Function for Double Hashing 
********************************************/
int HashFunc2( int Value ){
	
	return 3 - (Value % 3);
	
}


#endif

