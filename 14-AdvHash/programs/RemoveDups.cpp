/**********************************************
* File: RemoveDups.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is the main driver program for Problem 2
* in Lecture 12 
**********************************************/

#include "../classes/DynArr.h"
#include "../classes/DoubleHash.h"
#include <cstdlib>

int main(int argc, char** argv){

	DynArr<char> theArray;
	DoubleHash<char, bool> theHash;

	if(argc != 2){
		std::cerr << "Inputs should be \"./RemoveDups [String]\"\n"; 
		exit(-1);
	}

	// Put characters from argv into the Dynamic Array
	for(unsigned int iter = 0; ( *(argv + 1) )[iter] != (char)0; iter++){
		
		if( !theHash.contains(argv[1][iter]) ){
			theArray.push_back( ( *(argv + 1) )[iter] );
			theHash.insert( { *( ( *(argv + 1) )+ iter ), true} );
		}
	}
	
	std::cout << "Removing duplicates from " << argv[1] << " gives " << theArray << std::endl;

}
